#!/usr/bin/env python3
class Memory(object):
    MEMORY_INPUT = 8
    MEMORY_OUTPUT = 9
    MEMORY_SIZE = 10
    one_hot_lut = [1, 2, 4, 8]
    def __init__(self, memory=None):
        if memory == None:
            self.mem_cells = [0 for i in range(Memory.MEMORY_SIZE)]
        else:
            self.mem_cells = [i for i in memory]
            missing_cells = Memory.MEMORY_SIZE - len(self.mem_cells)
            for i in range(missing_cells):
                self.mem_cells.append(0)
    
    def process_address(self):
        for i in range(8):
            offset = self.mem_cells[Memory.MEMORY_INPUT] - self.mem_cells[i]
            if (offset >= 0 and offset < 4):
                tmp = '1'
                tmp += '{0:03b}'.format(i)
                tmp += '{0:04b}'.format(self.one_hot_lut[offset])
                self.mem_cells[Memory.MEMORY_OUTPUT] = int(tmp, 2)
                return
        self.mem_cells[Memory.MEMORY_OUTPUT] = self.mem_cells[Memory.MEMORY_INPUT]
    
    def print_mem(self):
        print("Address\tValue")
        for i in range(Memory.MEMORY_SIZE):
            print ("{}\t{}".format(i, self.mem_cells[i]))
    
    def set_value(self, value, address=None):
        if address == None:
            address = Memory.MEMORY_INPUT
        self.mem_cells[address] = value
    
def test_memory_class():
    mem1 = [4, 13, 22, 31, 37, 45, 77, 91, 42, 0]
    wz = Memory(mem1)
    wz.process_address()
    wz.print_mem()
    assert wz.mem_cells[wz.MEMORY_OUTPUT] == 42
    wz.set_value(33)
    wz.process_address()
    wz.print_mem()
    assert wz.mem_cells[wz.MEMORY_OUTPUT] == 180
