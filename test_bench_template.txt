-- {dateinfo}
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity {test_name} is
end {test_name};

architecture projecttb of {test_name} is
constant c_CLOCK_PERIOD		: time := 100 ns;
signal   tb_done		: std_logic;
signal   mem_address		: std_logic_vector (15 downto 0) := (others => '0');
signal   tb_rst	                : std_logic := '0';
signal   tb_start		: std_logic := '0';
signal   tb_clk		        : std_logic := '0';
signal   mem_o_data,mem_i_data	: std_logic_vector (7 downto 0);
signal   enable_wire  		: std_logic;
signal   mem_we		        : std_logic;
signal   write_ram_from_bench         : std_logic := '0'; --1 if forced
type ram_type is array (65535 downto 0) of std_logic_vector(7 downto 0);

signal c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,t0,t1,t2,t3,t4,t5,t6,t7,t8: std_logic_vector(7 downto 0) := (others => '0');

-- come da esempio su specifica
signal RAM: ram_type := (0 => std_logic_vector(to_unsigned( 4 , 8)),
                         1 => std_logic_vector(to_unsigned( 13 , 8)),
                         2 => std_logic_vector(to_unsigned( 22 , 8)),
                         3 => std_logic_vector(to_unsigned( 31 , 8)),
                         4 => std_logic_vector(to_unsigned( 37 , 8)),
                         5 => std_logic_vector(to_unsigned( 45 , 8)),
                         6 => std_logic_vector(to_unsigned( 77 , 8)),
                         7 => std_logic_vector(to_unsigned( 91 , 8)),
                         8 => std_logic_vector(to_unsigned( 42 , 8)),
			 others => (others =>'0'));

component project_reti_logiche is
port (
      i_clk         : in  std_logic;
      i_start       : in  std_logic;
      i_rst         : in  std_logic;
      i_data        : in  std_logic_vector(7 downto 0);
      o_address     : out std_logic_vector(15 downto 0);
      o_done        : out std_logic;
      o_en          : out std_logic;
      o_we          : out std_logic;
      o_data        : out std_logic_vector (7 downto 0)
      );
end component project_reti_logiche;


begin
c0 <= RAM(0);
c1 <= RAM(1);
c2 <= RAM(2);
c3 <= RAM(3);
c4 <= RAM(4);
c5 <= RAM(5);
c6 <= RAM(6);
c7 <= RAM(7);
c8 <= RAM(8);
c9 <= RAM(9);
c10 <= RAM(10);
UUT: project_reti_logiche
port map (
          i_clk      	=> tb_clk,
          i_start       => tb_start,
          i_rst      	=> tb_rst,
          i_data    	=> mem_o_data,
          o_address  	=> mem_address,
          o_done      	=> tb_done,
          o_en   	=> enable_wire,
          o_we 		=> mem_we,
          o_data    	=> mem_i_data
          );

p_CLK_GEN : process is
begin
    wait for c_CLOCK_PERIOD/2;
    tb_clk <= not tb_clk;
end process p_CLK_GEN;


MEM : process(tb_clk, write_ram_from_bench)
begin
    if write_ram_from_bench'event and write_ram_from_bench = '1' then
        RAM <= (
            0 => t0,
            1 => t1,
            2 => t2,
            3 => t3,
            4 => t4,
            5 => t5,
            6 => t6,
            7 => t7,
            8 => t8,
            others => (others =>'0')
        );
    elsif tb_clk'event and tb_clk = '1' then
        if enable_wire = '1' then
            if mem_we = '1' then
                RAM(conv_integer(mem_address))  <= mem_i_data;
                mem_o_data                      <= mem_i_data after 1 ns;
            else
                mem_o_data <= RAM(conv_integer(mem_address)) after 1 ns;
            end if;
        end if;
    end if;
end process;


test : process is
begin 
{content}
    assert false report "Simulation Ended!, TEST PASSATO" severity failure;
    wait;
end process test;

end projecttb; 
