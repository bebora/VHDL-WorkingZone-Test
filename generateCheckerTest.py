#!/usr/bin/env python3
from workingZone import *
from random import randint
N_TEST = 500
MAX_DIST = 32


memory = [255, 255, 255, 255, 255, 255, 255, 255, 0, 0]
machine = Memory(memory)
output = ""
for i in range(0, N_TEST):
    zone_addr = randint(0, 127)
    addr = zone_addr + randint(-MAX_DIST, MAX_DIST)
    if addr < 0:
        addr = 0
    elif addr > 127:
        addr = 127
    machine.set_value(zone_addr, 0)
    machine.set_value(addr)
    machine.process_address()
    result = machine.mem_cells[machine.MEMORY_OUTPUT]
    # print ('{:08b}'.format(result), result)
    valid = int(result/128)
    if valid:
        onehot = '{:04b}'.format(result%16)
    else:
        onehot = '0000'
    output += f"""
    wait for c_CLOCK_PERIOD;
    zone_addr <= std_logic_vector(to_unsigned({zone_addr}, 8));
    addr <= std_logic_vector(to_unsigned({addr}, 8));
    wait for c_CLOCK_PERIOD;
    assert valid = '{valid}' report "TEST FALLITO. Expected  '{valid}'  found " & std_logic'image(valid)  severity failure;
    assert offset = "{onehot}" report "TEST FALLITO. Expected  '{onehot}'  found " & integer'image(to_integer(unsigned(offset)))  severity failure;
    """

print(output)
