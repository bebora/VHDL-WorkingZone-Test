#!/usr/bin/env python3
from workingZone import *
from random import randint
from datetime import datetime
N_TEST = 3000


# datetime object containing current date and time
now = datetime.now()
dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")

with open("test_bench_template.txt", "r") as fp:
    template = fp.read()

memory = [255, 255, 255, 255, 255, 255, 255, 255, 0, 0]
machine = Memory(memory)
output = ""

def populate_ram(machine, ram=None):
    if ram is not None:
        machine.mem_cells = [i for i in ram]
    else:
        machine.mem_cells = [randint(0, 127) for _ in range(0, 10)]
    out = ""
    for i in range(0,9):
        out += "        t{} <= std_logic_vector(to_unsigned({}, 8));\n".format(i, machine.mem_cells[i])
    
    return f"""
    {out}
    write_ram_from_bench <= '1';
    wait for c_CLOCK_PERIOD/2;
    write_ram_from_bench <= '0';
    wait for c_CLOCK_PERIOD/2;
    """

def regular_processing(machine):
    out = ""
    machine.process_address()
    result = machine.mem_cells[machine.MEMORY_OUTPUT]
    binary_result = '{:08b}'.format(result)
    return out + f"""
    -- calculating result
    wait for c_CLOCK_PERIOD;
    tb_start <= '1';
    wait for c_CLOCK_PERIOD;
    wait until tb_done = '1';
    wait for c_CLOCK_PERIOD;
    tb_start <= '0';
    wait until tb_done = '0';
    wait for 100 ns;
    -- expected {binary_result}
    assert RAM(9) = std_logic_vector(to_unsigned( {result} , 8)) report "TEST FALLITO. Expected  {result}  found " & integer'image(to_integer(unsigned(RAM(9))))  severity failure;
    
    """
    
def check_tb_done_processing(machine):
    out = ""
    machine.process_address()
    result = machine.mem_cells[machine.MEMORY_OUTPUT]
    binary_result = '{:08b}'.format(result)
    return out + f"""
    -- testing tb_done correctly set to '0'
    wait for c_CLOCK_PERIOD;
    tb_start <= '1';
    wait for c_CLOCK_PERIOD;
    wait until tb_done = '1';
    wait for c_CLOCK_PERIOD;
    tb_start <= '0';
    wait for c_CLOCK_PERIOD*2;
    assert tb_done = '0' report "tb_done non abbassato entro due cicli di clock";
    -- expected {binary_result}
    assert RAM(9) = std_logic_vector(to_unsigned( {result} , 8)) report "TEST FALLITO. Expected  {result}  found " & integer'image(to_integer(unsigned(RAM(9))))  severity failure;
    
    """

def early_check_processing(machine):
    out = ""
    machine.process_address()
    result = machine.mem_cells[machine.MEMORY_OUTPUT]
    binary_result = '{:08b}'.format(result)
    return out + f"""
    -- testing RAM actually written
    wait for c_CLOCK_PERIOD;
    tb_start <= '1';
    wait for c_CLOCK_PERIOD;
    wait until tb_done = '1';
    -- expected {binary_result}
    assert RAM(9) = std_logic_vector(to_unsigned( {result} , 8)) report "TEST FALLITO. Expected  {result}  found " & integer'image(to_integer(unsigned(RAM(9))))  severity failure;
    wait for c_CLOCK_PERIOD;
    tb_start <= '0';
    wait until tb_done = '0';
    wait for 100 ns;
    
    """
    
def change_input(machine, value=None):
    if value is not None:
        new_value = value
    else:
        new_value = randint(0, 127)
    machine.set_value(new_value)
    return f"""
    -- changing test input
    t8 <= std_logic_vector(to_unsigned({new_value}, 8));
    write_ram_from_bench <= '1';
    wait for c_CLOCK_PERIOD/2;
    write_ram_from_bench <= '0';
    wait for c_CLOCK_PERIOD/2;
    """
    
def reset_machine(machine, new_ram=None):
    out = """
    -- sending a reset signal
    wait for 100 ns;
    wait for c_CLOCK_PERIOD;
    tb_rst <= '1';
    wait for c_CLOCK_PERIOD;"""
    out += populate_ram(machine, new_ram)
    out += """
    tb_rst <= '0';
    """
    return out

options = [reset_machine, change_input, regular_processing]

# Edge case 1: Alcune workingZone coincidenti
output += reset_machine(machine, [0, 11, 16, 16, 31, 45, 126, 99, 70, 0])
output += regular_processing(machine)
output += change_input(machine, 32)
output += regular_processing(machine)
output += change_input(machine, 17)

# Edge case 2: Limiti RAM
output += reset_machine(machine, [0, 11, 16, 82, 31, 45, 127, 99, 70, 0])
output += regular_processing(machine)
output += change_input(machine, 0)
output += regular_processing(machine)
output += change_input(machine, 127)
output += regular_processing(machine)

# Edge case 3: workingZone sovrappposte
output += reset_machine(machine, [0, 11, 16, 82, 84, 45, 120, 99, 70, 0])
output += regular_processing(machine)
output += change_input(machine, 82)
output += regular_processing(machine)
output += change_input(machine, 84)
output += regular_processing(machine)
output += change_input(machine, 86)
output += regular_processing(machine)

# Edge case 4: Indirizzo in input non valido (>127)
output += reset_machine(machine, [0, 11, 16, 30, 84, 45, 127, 99, 128, 0])
output += regular_processing(machine)
output += change_input(machine, 131)
output += regular_processing(machine)

# Edge case 5: WorkingZone e input fuori dai limiti
output += reset_machine(machine, [0, 11, 16, 30, 84, 45, 140, 99, 128, 0])
output += regular_processing(machine)
output += change_input(machine, 142)
output += regular_processing(machine)

# Edge case 6: Output davvero scritto in memoria quando o_done viene posto a '1'
output += reset_machine(machine, [0, 11, 16, 30, 84, 45, 127, 99, 2, 0])
output += early_check_processing(machine)
output += change_input(machine, 9)
output += early_check_processing(machine)

# Edge case 7: Segnale o_done abbassato abbastanze velocemente senza perdere eccessivi cicli di clock
output += reset_machine(machine, [0, 11, 16, 30, 84, 45, 127, 99, 4, 0])
output += check_tb_done_processing(machine)
output += change_input(machine, 13)
output += check_tb_done_processing(machine)

output += reset_machine(machine)
for i in range(0, N_TEST):
    output += options[randint(0,len(options)-1)](machine)

print(template.format(dateinfo=dt_string, test_name="wholetest", content=output))
